﻿using System;
using BJ.Edu.ProductService.Core.Dto;
using MediatR;

namespace BJ.Edu.ProductService.Core.Queries
{
    public class GetProductVariantQuery : IRequest<ProductVariantDto>
    {
        public Guid ProductId { get; set; }

        public Guid VariantId { get; set; }
    }
}
