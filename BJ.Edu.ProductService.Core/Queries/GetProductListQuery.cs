﻿using System.Collections.Generic;
using BJ.Edu.ProductService.Core.Entities;
using MediatR;

namespace BJ.Edu.ProductService.Core.Queries
{
    public class GetProductListQuery : IRequest<IEnumerable<Product>>
    {
    }
}
