﻿using BJ.Edu.ProductService.Core.Dto;
using MediatR;
using System;

namespace BJ.Edu.ProductService.Core.Queries
{
    public class GetProductQuery : IRequest<ProductDto>
    {
        public Guid ProductId { get; set; }
    }
}
