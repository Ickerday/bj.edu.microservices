﻿using System;

namespace BJ.Edu.ProductService.Core.Commands
{
    public class CreateProductVariantResult
    {
        public Guid ProductVariantId { get; set; }
    }
}
