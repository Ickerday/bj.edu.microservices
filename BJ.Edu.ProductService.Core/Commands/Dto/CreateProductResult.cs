﻿using System;

namespace BJ.Edu.ProductService.Core.Commands
{
    public class CreateProductResult
    {
        public Guid ProductId { get; set; }
    }
}
