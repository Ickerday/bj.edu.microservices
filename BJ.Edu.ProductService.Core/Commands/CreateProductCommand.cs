﻿using BJ.Edu.ProductService.Core.Dto;
using MediatR;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BJ.Edu.ProductService.Core.Commands
{
    public class CreateProductCommand : IRequest<CreateProductResult>
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        public IEnumerable<ProductImageDto> Images { get; set; } = new List<ProductImageDto>();
    }
}
