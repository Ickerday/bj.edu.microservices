﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BJ.Edu.ProductService.Core.Entities;
using MediatR;

namespace BJ.Edu.ProductService.Core.Commands
{
    public class CreateProductVariantCommand : IRequest<CreateProductVariantResult>
    {
        [Required]
        public Guid ProductId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        public IEnumerable<ProductImage> Images { get; set; } = new List<ProductImage>();
    }
}
