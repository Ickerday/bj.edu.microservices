﻿using BJ.Edu.ProductService.Core.Commands.Dto;
using BJ.Edu.ProductService.Core.Dto;
using MediatR;
using System;
using System.Collections.Generic;

namespace BJ.Edu.ProductService.Core.Commands
{
    public class UpdateImagesCommand : IRequest<UpdateImagesResult>
    {
        public Guid ProductVariantId { get; set; }

        public IEnumerable<ProductImageDto> Images { get; set; } = new List<ProductImageDto>();
    }
}
