﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BJ.Edu.ProductService.Core.Entities
{
    public class User
    {
        [Key]
        [Required]
        public Guid ClientId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Address { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }
    }
}
