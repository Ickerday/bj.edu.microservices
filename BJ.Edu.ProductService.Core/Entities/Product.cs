﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BJ.Edu.ProductService.Core.Entities
{
    public class Product
    {
        [Key]
        [Required]
        public Guid ProductId { get; set; } = Guid.NewGuid();

        // A valid product has at least one variant
        public IList<ProductVariant> Variants { get; set; } = new List<ProductVariant>();

        [Required]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Required]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
