﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BJ.Edu.ProductService.Core.Entities
{
    public class ProductImage
    {
        [Key]
        [Required]
        public Guid ProductImageId { get; set; } = Guid.NewGuid();

        [Required]
        public string ImageUrl { get; set; }

        public string AltText { get; set; }

        public uint Order { get; set; }
    }
}
