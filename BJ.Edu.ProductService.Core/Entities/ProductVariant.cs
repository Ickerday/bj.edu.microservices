﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BJ.Edu.ProductService.Core.Entities
{
    public class ProductVariant
    {
        [Key]
        [Required]
        public Guid ProductVariantId { get; set; } = Guid.NewGuid();

        [Required]
        public Product Product { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(2000)]
        public string Name { get; set; }

        [MaxLength(20000)]
        public string Description { get; set; }

        [Required]
        public bool IsDefault { get; set; }

        [Required]
        public decimal Price { get; set; }

        public IEnumerable<ProductImage> Images { get; set; } = new List<ProductImage>();

        [Required]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Required]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
