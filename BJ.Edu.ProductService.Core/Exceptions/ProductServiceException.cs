﻿using System;
using System.Runtime.Serialization;

namespace BJ.Edu.ProductService.Core.Exceptions
{
    /// <inheritdoc />
    public class ProductServiceException : Exception
    {
        public ProductServiceException() { }

        public ProductServiceException(string message) : base(message) { }

        public ProductServiceException(string message, Exception innerException) : base(message, innerException) { }

        protected ProductServiceException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
