﻿using System.Collections.Generic;

namespace BJ.Edu.ProductService.Core.Dto
{
    public class ProductDto
    {
        public IEnumerable<ProductVariantDto> Variants = new List<ProductVariantDto>();
    }
}
