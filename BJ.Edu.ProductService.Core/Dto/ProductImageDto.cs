﻿using System;

namespace BJ.Edu.ProductService.Core.Dto
{
    public class ProductImageDto
    {
        public Guid ProductImageId { get; set; }
    }
}
