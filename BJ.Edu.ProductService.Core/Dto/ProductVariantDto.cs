﻿using System;
using System.Collections.Generic;

namespace BJ.Edu.ProductService.Core.Dto
{
    public class ProductVariantDto
    {
        public Guid ProductId { get; set; }

        public Guid ProductVariantId { get; set; }

        public IEnumerable<ProductImageDto> Images { get; set; }
    }
}
