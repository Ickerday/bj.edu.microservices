﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using BJ.Edu.PurchaseService.Core.Commands;
using BJ.Edu.PurchaseService.Core.Queries;

namespace BJ.Edu.PurchaseService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PurchaseController : ControllerBase
    {
        private readonly IMediator _bus;
        private readonly ILogger<PurchaseController> _logger;

        public PurchaseController(IMediator bus, ILogger<PurchaseController> logger)
        {
            _bus = bus;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetRecentPurchases([FromQuery]int skip, [FromQuery]int take)
        {
            _logger.LogInformation(nameof(GetRecentPurchases));

            var query = new GetRecentPurchasesQuery
            {
                Skip = skip,
                Take = take
            };
            var result = await _bus.Send(query);
            var response = new JsonResult(result);
            return response;
        }

        [HttpPost]
        public async Task<IActionResult> PurchaseProducts([FromBody]PurchaseProductsCommand cmd)
        {
            _logger.LogInformation(nameof(PurchaseProducts));

            var result = await _bus.Send(cmd);
            var response = new JsonResult(result);
            return response;
        }
    }
}
