﻿using BJ.Edu.Infrastructure;
using BJ.Edu.PurchaseService.Core.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BJ.Edu.PurchaseService.Core.Queries;

namespace BJ.Edu.PurchaseService.Queries
{
    public class GetRecentPurchasesQueryHandler : IRequestHandler<GetRecentPurchasesQuery, IEnumerable<Purchase>>
    {
        private readonly MicroserviceContext _context;
        private readonly ILogger<GetRecentPurchasesQueryHandler> _logger;

        public GetRecentPurchasesQueryHandler(MicroserviceContext context, ILogger<GetRecentPurchasesQueryHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<IEnumerable<Purchase>> Handle(GetRecentPurchasesQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation(nameof(GetRecentPurchasesQueryHandler));

            var results = _context.Purchases.Skip(request.Skip).Take(request.Take);

            return await Task.FromResult(results);
        }
    }
}
