﻿using System;
using System.ComponentModel.DataAnnotations;
using BJ.Edu.SupplyService.Core.Entities;
using MediatR;

namespace BJ.Edu.SupplyService.Core.Commands
{
    public class AddProductSupplyCommand : IRequest<Supply>
    {
        [Required]
        public Guid SupplierId { get; set; }

        [Required]
        public Guid ProductVariantId { get; set; }

        [Required]
        public ulong Quantity { get; set; }
    }
}
