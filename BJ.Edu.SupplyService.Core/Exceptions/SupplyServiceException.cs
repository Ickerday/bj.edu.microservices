﻿using System;
using System.Runtime.Serialization;

namespace BJ.Edu.SupplyService.Core.Exceptions
{
    /// <inheritdoc />
    public class SupplyServiceException : Exception
    {
        public SupplyServiceException() { }

        public SupplyServiceException(string message) : base(message) { }

        public SupplyServiceException(string message, Exception innerException) : base(message, innerException) { }

        protected SupplyServiceException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
