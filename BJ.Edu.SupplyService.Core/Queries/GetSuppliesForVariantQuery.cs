﻿using System.Collections.Generic;
using BJ.Edu.SupplyService.Core.Entities;
using MediatR;

namespace BJ.Edu.SupplyService.Core.Queries
{
    public class GetSuppliesForVariantQuery : IRequest<IEnumerable<Supply>>
    {
    }
}
