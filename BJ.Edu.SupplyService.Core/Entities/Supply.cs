﻿using BJ.Edu.ProductService.Core.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace BJ.Edu.SupplyService.Core.Entities
{
    public class Supply
    {
        [Key]
        [Required]
        public Guid SupplyId { get; set; }

        [Required]
        public ProductVariant Variant { get; set; }

        [Required]
        public ulong Quantity { get; set; }
    }
}
