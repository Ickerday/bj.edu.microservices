﻿using BJ.Edu.SupplyService.Core.Dto;
using BJ.Edu.SupplyService.Core.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using BJ.Edu.SupplyService.Core.Commands;

namespace BJ.Edu.SupplyService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SupplyController : ControllerBase
    {
        private readonly IMediator _bus;
        private readonly ILogger<SupplyController> _logger;

        public SupplyController(IMediator bus, ILogger<SupplyController> logger)
        {
            _bus = bus;
            _logger = logger;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Supply))]
        public async Task<IActionResult> AddProductSupply([FromBody]AddProductSupplyCommand command)
        {
            _logger.LogInformation(nameof(AddProductSupply));

            var result = await _bus.Send(command);
            var response = new JsonResult(result);
            return response;
        }


    }
}
