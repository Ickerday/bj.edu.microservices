﻿using BJ.Edu.Infrastructure;
using BJ.Edu.SupplyService.Core.Dto;
using BJ.Edu.SupplyService.Core.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BJ.Edu.SupplyService.Core.Queries;

namespace BJ.Edu.SupplyService.Queries
{
    public class GetSuppliesForVariantQueryHandler : IRequestHandler<GetSuppliesForVariantQuery, IEnumerable<Supply>>
    {
        private readonly MicroserviceContext _context;
        private readonly ILogger<GetSuppliesForVariantQueryHandler> _logger;

        public Task<IEnumerable<Supply>> Handle(GetSuppliesForVariantQuery request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
