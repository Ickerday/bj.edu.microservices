﻿using BJ.Edu.Infrastructure;
using BJ.Edu.SupplyService.Core.Dto;
using BJ.Edu.SupplyService.Core.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using BJ.Edu.SupplyService.Core.Commands;

namespace BJ.Edu.SupplyService.Commands
{
    public class AddProductSupplyCommandHandler : IRequestHandler<AddProductSupplyCommand, Supply>
    {
        private readonly MicroserviceContext _context;
        private readonly ILogger<AddProductSupplyCommandHandler> _logger;

        public async Task<Supply> Handle(AddProductSupplyCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
