﻿using System.Collections.Generic;
using BJ.Edu.PurchaseService.Core.Entities;
using MediatR;

namespace BJ.Edu.PurchaseService.Core.Queries
{
    public class GetRecentPurchasesQuery : IRequest<IEnumerable<Purchase>>
    {
        public int Skip { get; set; }

        public int Take { get; set; }
    }
}
