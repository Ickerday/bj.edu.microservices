﻿using BJ.Edu.PurchaseService.Core.Entities;
using MediatR;

namespace BJ.Edu.PurchaseService.Core.Commands
{
    public class PurchaseProductsCommand : IRequest<Purchase>
    {
    }
}
