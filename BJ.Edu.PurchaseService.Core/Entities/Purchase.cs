﻿using BJ.Edu.ProductService.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BJ.Edu.PurchaseService.Core.Entities
{
    public class Purchase
    {
        [Key]
        [Required]
        public Guid PurchaseId { get; set; }

        public IEnumerable<ProductVariant> Basket { get; set; } = new List<ProductVariant>();


    }
}
