﻿using BJ.Edu.ProductService.Core.Entities;
using BJ.Edu.PurchaseService.Core.Entities;
using BJ.Edu.SupplyService.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace BJ.Edu.Infrastructure
{
    public class MicroserviceContext : DbContext
    {
        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<ProductVariant> Variants { get; set; }

        public virtual DbSet<ProductImage> Images { get; set; }

        public virtual DbSet<Supply> Supllies { get; set; }

        public virtual DbSet<Purchase> Purchases { get; set; }

        public MicroserviceContext(DbContextOptions options) : base(options) => Database?.Migrate();

    }
}
