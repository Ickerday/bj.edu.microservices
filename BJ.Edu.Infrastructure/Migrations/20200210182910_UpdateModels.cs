﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BJ.Edu.Infrastructure.Migrations
{
    public partial class UpdateModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Images_Products_ProductId",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "IX_Images_ProductId",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Variants");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "Products");

            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "Variants",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "PurchaseId",
                table: "Variants",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AltText",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProductVariantId",
                table: "Images",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Purchases",
                columns: table => new
                {
                    PurchaseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Purchases", x => x.PurchaseId);
                });

            migrationBuilder.CreateTable(
                name: "Supllies",
                columns: table => new
                {
                    SupplyId = table.Column<Guid>(nullable: false),
                    VariantProductVariantId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supllies", x => x.SupplyId);
                    table.ForeignKey(
                        name: "FK_Supllies_Variants_VariantProductVariantId",
                        column: x => x.VariantProductVariantId,
                        principalTable: "Variants",
                        principalColumn: "ProductVariantId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Variants_PurchaseId",
                table: "Variants",
                column: "PurchaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_ProductVariantId",
                table: "Images",
                column: "ProductVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_Supllies_VariantProductVariantId",
                table: "Supllies",
                column: "VariantProductVariantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Images_Variants_ProductVariantId",
                table: "Images",
                column: "ProductVariantId",
                principalTable: "Variants",
                principalColumn: "ProductVariantId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Variants_Purchases_PurchaseId",
                table: "Variants",
                column: "PurchaseId",
                principalTable: "Purchases",
                principalColumn: "PurchaseId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Images_Variants_ProductVariantId",
                table: "Images");

            migrationBuilder.DropForeignKey(
                name: "FK_Variants_Purchases_PurchaseId",
                table: "Variants");

            migrationBuilder.DropTable(
                name: "Purchases");

            migrationBuilder.DropTable(
                name: "Supllies");

            migrationBuilder.DropIndex(
                name: "IX_Variants_PurchaseId",
                table: "Variants");

            migrationBuilder.DropIndex(
                name: "IX_Images_ProductVariantId",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "Variants");

            migrationBuilder.DropColumn(
                name: "PurchaseId",
                table: "Variants");

            migrationBuilder.DropColumn(
                name: "AltText",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "ProductVariantId",
                table: "Images");

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Variants",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Products",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Products",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "Products",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_Images_ProductId",
                table: "Images",
                column: "ProductId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Images_Products_ProductId",
                table: "Images",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
