﻿using System;

namespace BJ.Edu.Shared
{
    public static class ConnectionStringResolver
    {
        public static string ResolveFromEnv()
        {
            var pgHost = Environment.GetEnvironmentVariable("PGHOST") ?? "localhost";
            var pgPort = Environment.GetEnvironmentVariable("PGPORT") ?? "25432";
            var pgUser = Environment.GetEnvironmentVariable("PGUSER") ?? "postgres";
            var pgPassword = Environment.GetEnvironmentVariable("PGPASSWORD") ?? "postgres";

            var connectionString = $"Host={pgHost};Port={pgPort};User ID={pgUser};Password={pgPassword}";
            return connectionString;
        }
    }
}
