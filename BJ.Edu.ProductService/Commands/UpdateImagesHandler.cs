﻿using BJ.Edu.Infrastructure;
using BJ.Edu.ProductService.Core.Commands;
using BJ.Edu.ProductService.Core.Commands.Dto;
using BJ.Edu.ProductService.Core.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BJ.Edu.ProductService.Commands
{
    public class UpdateImagesHandler : IRequestHandler<UpdateImagesCommand, UpdateImagesResult>
    {
        private readonly MicroserviceContext _context;
        private readonly ILogger<UpdateImagesHandler> _logger;

        public UpdateImagesHandler(MicroserviceContext context, ILogger<UpdateImagesHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<UpdateImagesResult> Handle(UpdateImagesCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation(nameof(UpdateImagesHandler));

            var variant = await _context.Variants
                .SingleOrDefaultAsync(pv => pv.ProductVariantId == request.ProductVariantId, cancellationToken);

            var newImages = request.Images.Select(imageDto => new ProductImage
            {
                // TODO
            });

            var result = new UpdateImagesResult
            {
                // TODO
            };
            return result;
        }
    }
}
