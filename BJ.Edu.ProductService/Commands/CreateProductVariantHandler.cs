using BJ.Edu.Infrastructure;
using BJ.Edu.ProductService.Core.Commands;
using BJ.Edu.ProductService.Core.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BJ.Edu.ProductService.Commands
{
    public class CreateProductVariantHandler : IRequestHandler<CreateProductVariantCommand, CreateProductVariantResult>
    {
        private readonly MicroserviceContext _context;
        private readonly ILogger<CreateProductVariantHandler> _logger;

        public CreateProductVariantHandler(MicroserviceContext context, ILogger<CreateProductVariantHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<CreateProductVariantResult> Handle(CreateProductVariantCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation(nameof(CreateProductVariantHandler));
            if (request == null || request.ProductId == Guid.Empty)
                throw new ArgumentNullException(nameof(request));

            var product = await _context.Products.SingleOrDefaultAsync(p => p.ProductId == request.ProductId, cancellationToken);
            if (product == null)
                throw new ArgumentNullException(nameof(product));

            var variant = new ProductVariant();
            variant.Name = request.Name;
            variant.Description = request.Description;
            variant.Price = request.Price;
            variant.IsDefault = !product.Variants.Any();
            variant.Images = request.Images;

            product.Variants.Add(variant);
            _context.Products.Update(product);
            await _context.SaveChangesAsync(cancellationToken);

            var result = new CreateProductVariantResult
            {
                ProductVariantId = variant.ProductVariantId
            };
            return result;
        }
    }
}
