﻿using BJ.Edu.Infrastructure;
using BJ.Edu.ProductService.Core.Commands;
using BJ.Edu.ProductService.Core.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BJ.Edu.ProductService.Commands
{
    public class CreateProductHandler : IRequestHandler<CreateProductCommand, CreateProductResult>
    {
        private readonly MicroserviceContext _context;
        private readonly ILogger<CreateProductHandler> _logger;

        public CreateProductHandler(MicroserviceContext context, ILogger<CreateProductHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<CreateProductResult> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation(nameof(CreateProductHandler));
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var product = new Product();
            var defaultVariant = new ProductVariant();
            defaultVariant.Product = product;
            defaultVariant.Name = request.Name;
            defaultVariant.Description = request.Description;
            defaultVariant.Price = request.Price;
            defaultVariant.IsDefault = !product.Variants.Any();
            defaultVariant.Images = request.Images;

            product.Variants.Add(defaultVariant);
            await _context.Products.AddAsync(product, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            var result = new CreateProductResult();
            result.ProductId = product.ProductId;
            return result;
        }
    }
}
