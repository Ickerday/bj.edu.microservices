﻿using BJ.Edu.ProductService.Core.Dto;
using BJ.Edu.ProductService.Core.Queries;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BJ.Edu.ProductService.Queries
{
    public class GetProductHandler : IRequestHandler<GetProductQuery, ProductDto>
    {
        public Task<ProductDto> Handle(GetProductQuery request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
