﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using BJ.Edu.ProductService.Core.Entities;
using BJ.Edu.ProductService.Core.Queries;

namespace BJ.Edu.ProductService.Queries
{
    public class GetProductVariantHandler : IRequestHandler<GetProductVariantQuery, ProductVariant>
    {
        public Task<ProductVariant> Handle(GetProductVariantQuery request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
