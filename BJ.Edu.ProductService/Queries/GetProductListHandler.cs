﻿using BJ.Edu.Infrastructure;
using BJ.Edu.ProductService.Core.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BJ.Edu.ProductService.Core.Queries;

namespace BJ.Edu.ProductService.Queries
{
    public class GetProductListHandler : IRequestHandler<GetProductListQuery, IEnumerable<Product>>
    {
        private readonly MicroserviceContext _context;
        private readonly ILogger<GetProductListHandler> _logger;

        public GetProductListHandler(MicroserviceContext context, ILogger<GetProductListHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<IEnumerable<Product>> Handle(GetProductListQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation(nameof(GetProductListHandler));

            var result = await _context.Products.ToListAsync(cancellationToken).ConfigureAwait(false);
            return result;
        }
    }
}
