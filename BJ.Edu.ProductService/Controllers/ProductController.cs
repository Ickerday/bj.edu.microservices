﻿using BJ.Edu.ProductService.Core.Commands;
using BJ.Edu.ProductService.Core.Entities;
using BJ.Edu.ProductService.Core.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BJ.Edu.ProductService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IMediator _bus;
        private readonly ILogger<ProductController> _logger;

        public ProductController(IMediator bus, ILogger<ProductController> logger)
        {
            _bus = bus;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Product>))]
        public async Task<IActionResult> GetProductList()
        {
            _logger.LogInformation(nameof(GetProductList));

            var query = new GetProductListQuery();

            var response = await _bus.Send(query);
            var result = new JsonResult(response);
            return result;
        }

        [HttpGet("{productId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Product))]
        public async Task<IActionResult> GetProduct([FromRoute]string productId)
        {
            _logger.LogInformation(nameof(GetProduct));

            var query = new GetProductQuery();
            query.ProductId = Guid.Parse(productId);

            var response = await _bus.Send(query);
            var result = new JsonResult(response);
            return result;
        }

        [HttpGet("{productId}/{variantId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductVariant))]
        public async Task<IActionResult> GetProductVariant([FromRoute]string productId, [FromRoute]string variantId)
        {
            _logger.LogInformation(nameof(GetProductVariant));

            var query = new GetProductVariantQuery();
            query.ProductId = Guid.Parse(productId);
            query.VariantId = Guid.Parse(variantId);

            var response = await _bus.Send(query);
            var result = new JsonResult(response);
            return result;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Product))]
        public async Task<IActionResult> CreateProduct([FromBody]CreateProductCommand cmd)
        {
            _logger.LogInformation(nameof(CreateProduct));

            var response = await _bus.Send(cmd);
            var result = new JsonResult(response);
            return result;
        }

        [HttpPost("{productId}/variant")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductVariant))]
        public async Task<IActionResult> CreateProductVariant([FromBody]CreateProductVariantCommand cmd)
        {
            _logger.LogInformation(nameof(CreateProductVariant));
            if (cmd == null)
                return new BadRequestResult();

            var response = await _bus.Send(cmd);
            var result = new JsonResult(response);
            return result;
        }

        [HttpPost("{productId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductVariant))]
        public async Task<IActionResult> UpdateImages([FromBody]UpdateImagesCommand cmd)
        {
            _logger.LogInformation(nameof(UpdateImages));

            if (cmd != null)
                return new BadRequestResult();

            var response = await _bus.Send(cmd);
            var result = new JsonResult(response);
            return result;
        }
    }
}
